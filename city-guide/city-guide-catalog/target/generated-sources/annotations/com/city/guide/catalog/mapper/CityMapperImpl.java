package com.city.guide.catalog.mapper;

import com.city.guide.catalog.dto.CityDto;
import com.city.guide.catalog.entity.neo.City;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-18T18:12:30+0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Oracle Corporation)"
)
@Component
public class CityMapperImpl implements CityMapper {

    @Override
    public CityDto toDto(City city) {
        if ( city == null ) {
            return null;
        }

        CityDto cityDto = new CityDto();

        cityDto.setId( city.getId() );
        cityDto.setName( city.getName() );

        return cityDto;
    }

    @Override
    public List<CityDto> toDtos(List<City> cities) {
        if ( cities == null ) {
            return null;
        }

        List<CityDto> list = new ArrayList<CityDto>( cities.size() );
        for ( City city : cities ) {
            list.add( toDto( city ) );
        }

        return list;
    }

    @Override
    public City toEntity(CityDto cityDto) {
        if ( cityDto == null ) {
            return null;
        }

        City city = new City();

        city.setId( cityDto.getId() );
        city.setName( cityDto.getName() );

        return city;
    }

    @Override
    public List<City> toEntities(List<CityDto> cityDtos) {
        if ( cityDtos == null ) {
            return null;
        }

        List<City> list = new ArrayList<City>( cityDtos.size() );
        for ( CityDto cityDto : cityDtos ) {
            list.add( toEntity( cityDto ) );
        }

        return list;
    }
}
