package com.city.guide.catalog.service.impl;

import com.city.guide.catalog.dto.CityDto;
import com.city.guide.catalog.entity.neo.City;
import com.city.guide.catalog.mapper.CityMapper;
import com.city.guide.catalog.repository.neo.CityRepository;
import com.city.guide.catalog.service.CityService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;
    private final CityMapper cityMapper;

    @Override
    public List<CityDto> getCities() {
        return cityMapper.toDtos((List<City>) cityRepository.findAll());
    }

    @Override
    public CityDto addCity(CityDto cityDto) {
        City city = cityRepository.save(cityMapper.toEntity(cityDto));
        return cityMapper.toDto(city);
    }
}
