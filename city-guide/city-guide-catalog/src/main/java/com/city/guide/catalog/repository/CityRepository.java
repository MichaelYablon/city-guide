package com.city.guide.catalog.repository;

import com.city.guide.catalog.entity.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends CrudRepository<Long, City> {
}
