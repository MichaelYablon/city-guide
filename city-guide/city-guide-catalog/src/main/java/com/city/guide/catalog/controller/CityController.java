package com.city.guide.catalog.controller;

import com.city.guide.catalog.dto.CityDto;
import com.city.guide.catalog.entity.neo.City;
import com.city.guide.catalog.service.CityService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("catalog")
@AllArgsConstructor
public class CityController {

    private final CityService cityService;
    @RequestMapping("/list.html")
    public ModelAndView ItemList() {
        return new ModelAndView("itemlist", "items", null);
    }
    @GetMapping
    public List<CityDto> getCities() {
        return cityService.getCities();
    }

    @GetMapping("/{cityId}")
    public CityDto getCity(@RequestParam Long cityId) {
        return new CityDto();
    }

    @PostMapping
    public CityDto addCity(@RequestBody CityDto cityDto) {
        return cityService.addCity(cityDto);
    }
}
