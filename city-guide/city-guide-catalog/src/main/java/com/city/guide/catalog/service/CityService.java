package com.city.guide.catalog.service;

import com.city.guide.catalog.dto.CityDto;

import java.util.List;

public interface CityService {

    List<CityDto> getCities();

    CityDto addCity(CityDto cityDto);
}
