package com.city.guide.catalog.mapper;

import com.city.guide.catalog.dto.CityDto;
import com.city.guide.catalog.entity.neo.City;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CityMapper {

    CityDto toDto(City city);

    List<CityDto> toDtos(List<City> cities);

    City toEntity(CityDto cityDto);

    List<City> toEntities(List<CityDto> cityDtos);
}
