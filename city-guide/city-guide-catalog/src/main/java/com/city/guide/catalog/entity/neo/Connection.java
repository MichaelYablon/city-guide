package com.city.guide.catalog.entity.neo;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "CONNECTION")
public class Connection {

    @StartNode
    City sourceCity;

    @EndNode
    City endCity;

    long timeToTravel;
}
