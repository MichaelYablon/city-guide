package com.city.guide.catalog.dto;

import lombok.Data;

@Data
public class CityDto {
    public Long id;
    public String name;
}
