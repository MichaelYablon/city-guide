package com.city.guide.catalog.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CITY")
public class City implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "connection_id", referencedColumnName = "id")
    private Connection connection;
}
