package com.city.guide.catalog.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CONNECTION")
public class Connection implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "connection")
    private City source;

    @OneToOne(mappedBy = "connection")
    private City destination;

    @Column(name = "time_between")
    private long timeToTravel;
}
