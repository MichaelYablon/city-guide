package com.city.guide.catalog.config;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.mapstruct.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@EnableJpaRepositories(
        entityManagerFactoryRef = "cityEntityManager",
        transactionManagerRef = "cityTransactionManager",
        basePackages = "com.city.guide.catalog.repository")
public class EntityManager  implements WebMvcConfigurer {

    private static final String PERSISTENCE_UNIT = "city";
    private static final String SHARED_ENTITY_PACKAGES = "com.city.guide.catalog.entity";

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean cityEntityManager(
            EntityManagerFactoryBuilder builder,
            DataSource cityDataSource) {
        return builder
                .dataSource(cityDataSource)
                .packages(SHARED_ENTITY_PACKAGES)
                .persistenceUnit(PERSISTENCE_UNIT)
                .build();
    }

    @Bean
    @Primary
    public JpaTransactionManager cityTransactionManager(
             EntityManagerFactory cityEntityManager) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(cityEntityManager);

        return transactionManager;
    }
}