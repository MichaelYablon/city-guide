package com.city.guide.catalog.entity.neo;

import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@Getter
@Setter
@NodeEntity
public class City {
    @Id
    @GeneratedValue
    private Long id;

    String name;

    @Relationship(type = "CONNECTION", direction = Relationship.INCOMING)
    Set<City> connections;
}
