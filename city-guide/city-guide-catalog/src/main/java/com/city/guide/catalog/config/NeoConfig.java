package com.city.guide.catalog.config;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableNeo4jRepositories(
        basePackages = "com.city.guide.catalog.repository.neo",
        sessionFactoryRef = "neoSessionFactory")
@EnableTransactionManagement
public class NeoConfig {

    @Bean
    public SessionFactory neoSessionFactory() {
        // with domain entity base package(s)
        return new SessionFactory(configuration(), "com.city.guide.catalog.entity.neo");
    }

    @Bean
    public org.neo4j.ogm.config.Configuration configuration() {
        return new org.neo4j.ogm.config.Configuration.Builder()
                .uri("bolt://localhost:7687")
                .credentials("neo4j", "test")
                .build();
    }

    @Bean
    public Neo4jTransactionManager transactionManager() {
        return new Neo4jTransactionManager(neoSessionFactory());
    }
}
