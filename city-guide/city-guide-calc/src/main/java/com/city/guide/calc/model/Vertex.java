package com.city.guide.calc.model;

import lombok.Data;

@Data
public class Vertex {
    final private String id;
    final private String name;
}
