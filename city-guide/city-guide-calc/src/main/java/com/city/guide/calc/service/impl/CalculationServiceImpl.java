package com.city.guide.calc.service.impl;

import com.city.guide.calc.model.Vertex;
import com.city.guide.calc.service.CalculationService;
import com.city.guide.calc.service.CatalogClientService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalculationServiceImpl implements CalculationService {

    private final CatalogClientService catalogClientService;

    public CalculationServiceImpl(CatalogClientService catalogClientService) {
        this.catalogClientService = catalogClientService;
    }

    @Override
    public long getShortestWayByTime(Long sourceId, Long destinationId) {
        Vertex sourceCity = catalogClientService.getVertex(sourceId);
        Vertex destinationCity = catalogClientService.getVertex(sourceId);
        return 0;
    }

    @Override
    public List<Long> getShortestWayByConnections(Long sourceId, Long destinationId) {
        return null;
    }
}
