package com.city.guide.calc.controller;

import com.city.guide.calc.service.CalculationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("calculation")
@AllArgsConstructor
public class CalcController {

    private final CalculationService calculationService;

    @GetMapping("/source/{sourceId}/destination/{destinationId}")
    public long getShortestWayByTime(@RequestParam Long sourceId, @RequestParam Long destinationId) {
        return calculationService.getShortestWayByTime(sourceId, destinationId);
    }
}
