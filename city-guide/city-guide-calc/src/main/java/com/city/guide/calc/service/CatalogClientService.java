package com.city.guide.calc.service;

import com.city.guide.calc.model.Vertex;

public interface CatalogClientService {
    Vertex getVertex(Long id);
}
