package com.city.guide.calc.service.impl;

import com.city.guide.calc.model.Vertex;
import com.city.guide.calc.service.CatalogClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CatalogClientServiceImpl implements CatalogClientService {

    private RestTemplate restTemplate;
    private String catalogUrl;

    @Autowired
    public CatalogClientServiceImpl(RestTemplate restTemplate,
                                    @Value("${catalog.service.host:catalog}") String catalogServiceHost,
                                    @Value("${catalog.service.port:8080}") long catalogServicePort) {
        this.restTemplate = restTemplate;
        this.catalogUrl = String.format("http://%s:%s/catalog/", catalogServiceHost, catalogServicePort);
    }

    @Override
    public Vertex getVertex(Long id) {
        return restTemplate.getForObject(catalogUrl + id, Vertex.class);
    }
}
