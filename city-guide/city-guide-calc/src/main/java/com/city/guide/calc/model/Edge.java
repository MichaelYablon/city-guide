package com.city.guide.calc.model;

import lombok.Data;

@Data
public class Edge  {
    private final String id;
    private final Vertex source;
    private final Vertex destination;
    private final int weight;
}