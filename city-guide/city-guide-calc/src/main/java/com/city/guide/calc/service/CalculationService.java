package com.city.guide.calc.service;

import java.util.List;

public interface CalculationService {

    long getShortestWayByTime(Long sourceId, Long destinationId);

    List<Long> getShortestWayByConnections(Long sourceId, Long destinationId);
}
